# Commands needed for demo

## Cloning and setting up credentials

`git config` sets your Git credentials

1. `cd Code`
2. `git clone YOUR-FORK`
3. `cd YOUR-FORK`
4. `code .` 
5. `git config user.name "NAME"`
6. `git config user.email "EMAIL"`

## Configuring my fork 

`git remote -v`: Shows URLs of remote repositories

1. `git remote -v` 
2. `git remote add upstream UPSTREAM-REPOSITORY`
3. `git remote -v`

## Change my file

1. `git pull upstream main`
   1. There might be a different upstream branch name
2. `git push origin main`
3. `git checkout -b branch-name`
4. `git status` 
5. `git add filename` 
   1. We can mention that `git add .` adds all modified files (be careful)
6. `git status`
7. `git commit -m "message"`
8. `git status` 
9. `git push origin branch-name`


# Git Demo Lesson Plan 

After you complete the Git terminology review with the students, you can demo the process of contributing to a project in GitLab.

## Objectives

By the end of this section, students will be able to:
* Use the fork and clone method to contribute to a Git repository
* Use the command line to complete Git tasks
* Create a directory to house their Git projects
* Apply knowledge learned from the Markdown pre-work to fix errors in written content

## Instructor setup tasks

Before you start the demo, complete the following tasks:

1. If you haven't set up your SSH key in GitLab, see [Using SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html).
2. Open a new tab and go to The Good Dogs Project website.
3. Open a new tab and go to The Good Dogs Project repository in GitLab.
4. Open a new terminal on your machine.
5. Create a directory named `code` on your local machine.

## Agenda

**1. Set introduction (2 minutes)**

Open The Good Dogs project website, and set the scene:
* The Good Dogs Project is located in California and helps owners raise happy and healthy dogs
* They just launched their new website, but there are errors in their blog posts
* The students are tasked with fixing errors in the Markdown to help the company fix their website
  * Every student will be assigned a blog post to fix

Explain that you will demo the process of contributing to a repository before the students complete their work.

**2. Assign yourself an issue (1 minute)**

Open The Good Dogs Project repository in GitLab:

1. In The Good Dogs Project repository, click **Issues**.
2. Find the instructor's issue, and assign yourself to it.

After you assign yourself the issue, read through it with the students. Click the link to the blog post you need to fix and discuss the error.
  

**3. Fork The Good Dogs Project repository** (2 minutes)
Fork The Good Dogs Project repository in GitLab:

1. In The Good Dogs Project repository, click the **Fork** icon.
2. Select your personal namespace for the project.
3. Select the **Public** visibility level.
4. Click **Fork project**.

Remind students that a fork is a copy of a repository that exists under your namespace.

**4. Clone your fork (2 minutes)**
Clone your fork to your local machine:

1. In The Good Dogs Project repository in GitLab, click **Clone**.
2. Copy the SSH URL.
3. On your local machine, navigate to the `code` directory. 
    Explain that this directory holds your Git projects. 
4. Run `git clone SSH-URL` to clone the repository to your local machine.

Reiterate that you cloned your **fork**, not the original repository.

**5. Set your Git credentials (2 minutes)**
Configure your Git username and email:

1. Open The Good Dogs Project repository on your local machine. 
2. In the VS Code terminal, run the following commands:
   ```bash
   git config user.name GITLAB-USERNAME
   git config user.email GITLAB-EMAIL
   ```
   Explain that `git config` is used to set Git configuration values. You can set these values at the global level (for all projects) or at the project level.

**6. Add a new remote repository (2 minutes)**
To add a new remote repository:

1. In the VS Code terminal, run the following command:
   ```bash
   git remote -v
   git remote add upstream UPSTREAM-URL
   git remote -v
   ```
The `git remote` command lets you create and view connections to other repositories.

When you run `git remote -v` for the second time, point out which repository is the **upstream** and which one is the **origin**.

**7. Sync your working repository (1 minute)**
To sync your working repositories:

1. In the VS Code terminal, run `git pull upstream main` to pull down any changes from the `main` branch in the upstream repository. 
2. Run `git push origin main` to push those changes to the `main` branch in your fork.

**8. Create a new branch (1 minute)**
Create a new branch for your fix:

1. In the VS Code terminal, run `git checkout -b BRANCH-NAME` to create a new branch. 

    The branch name should match the issue title.

After you create the branch, run a quick `git status` to make sure you are on the correct branch.

**9. Stage, add, and commit your fix (5 minutes)**  
After you make your fix, walk students through the process of staging and committing your fix:

1. In the VS Code terminal, run `git status` to see which files have been modified.
2. Run `git add` to stage the file.
    The `git add` command adds new or changed files in your working directory to the Git staging area.
3. Run `git commit -m "MESSAGE"` to commit your changes.
    Commit messages should indicate what changed and why. 

**10. Push your branch to your fork (1 minute)**
To push your branch:

1. Run `git status` to make sure you are on the correct branch. 
2. Run `git push origin YOUR-BRANCH` to push your branch to your fork.
   `git push` is how you push commits from a local repository to a Git repository.

**11. Open a merge request (1 minute)**  
To open your merge request:

1. In your fork in GitLab, click **Merge requests**.
2. Click **New merge request**. 
3. Select the source branch and target branch.
    Remind the students that the source branch is their branch, and the target branch is where they want to merge *to*. **Make sure they select the correct source branch and target branch**.
4. Click **Compare branches and continue**.
5. Fill out the merge request details.
   1. Add a title.
   2. In the description, provide a short summary of the change you made and link to the related issue. 
   3. Assign your co-trainer as a reviewer.
   4. Click **Create merge request**.

**12. Review your changes (1 minute)**

After your co-trainer merges in your changes, open the website created specifically for your cohort, and compare it against the base repository to show the changes.

## Next steps

After the demo is complete, tell the students that they will contribute to The Good Dogs Project repository after the break. 
